import React from 'react';
import { Outlet, Navigate } from 'react-router';
import User from '../../request/service/User';

export default () => {
    const user = User.get();

    if (!user) {
        return <Navigate to={'/connexion'} replace />;
    }

    return  <Outlet />;
};
