import React, { useState } from 'react';
import { HuePicker } from 'react-color';
import { Button } from '@mui/material';

const ColorPickerButton = ({ color = undefined, row = undefined, onChange }) => {
    const [displayColorPicker, setDisplayColorPicker] = useState(false);
    const [currentColor, setCurrentColor] = useState(color || getRandomColor());

    if (row) {
        row.color = currentColor;
    }

    const handleClick = () => {
        setDisplayColorPicker(!displayColorPicker);
    };

    const handleClose = () => {
        setDisplayColorPicker(false);
    };

    const handleChange = (newColor) => {
        if (row) {
            row.color = newColor.hex;
        }
        setCurrentColor(newColor.hex);
        onChange(newColor.hex);
    };

    function getRandomColor() {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    return (
        <div>
            <Button
                style={{ backgroundColor: currentColor, width: '30px', height: '30px' }}
                onClick={handleClick}
            />
            {displayColorPicker ? (
                <div style={{ position: 'absolute', zIndex: '2' }}>
                    <div style={{ position: 'fixed', top: '0px', right: '0px', bottom: '0px', left: '0px' }} onClick={handleClose} />
                    <HuePicker color={currentColor} onChange={handleChange} />
                </div>
            ) : null}
        </div>
    );
};

export default ColorPickerButton;
