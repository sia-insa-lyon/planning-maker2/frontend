import { Box, Button, Typography } from "@mui/material";

function NotFound() {
    return (
        <Box
            sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                minHeight: '100vh',
            }}
        >
            <Typography variant="h1">
                404
            </Typography>
            <Typography variant="h6">
                Cette page n'existe pas.
            </Typography>
            <Button variant="contained" href="/" sx={{ mt: 5 }}>Retour à l'accueil</Button>
        </Box>
    );
}

export default NotFound;
