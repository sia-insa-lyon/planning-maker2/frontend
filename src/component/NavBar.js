import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import LogoutIcon from '@mui/icons-material/Logout';
import { Box, Tooltip } from '@mui/material';
import { Link, Outlet, useNavigate } from 'react-router-dom';
import User from '../request/service/User';
import AccountCircle from '@mui/icons-material/AccountCircle';

export function Buttons() {
    const navigate = useNavigate();

    const handleLogout = () => {
        User.logout();
        navigate('/connexion');
    };

    return (
        <>
            <Tooltip title="Profil">
                <IconButton
                    size="large"
                    color="inherit"
                    component={Link}
                    to="/"
                >
                    <AccountCircle />
                </IconButton>
            </Tooltip>
            <Tooltip title="Déconnexion">
                <IconButton
                    size="large"
                    color="inherit"
                    sx={{ mr: 2 }}
                    onClick={handleLogout}
                >
                    <LogoutIcon />
                </IconButton>
            </Tooltip>
        </>
    );
}

/**
 * Horizontal navigation bar used on the home page.
 */
function NavBar() {

    return (
        <>
            <AppBar position="static">
                <Toolbar>
                    <Box sx={{ flexGrow: 1 }} />
                    <Buttons />
                </Toolbar>
            </AppBar>
            <Outlet />
        </>
    );
}

export default NavBar;
