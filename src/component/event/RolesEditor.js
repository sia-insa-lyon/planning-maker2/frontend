import Box from '@mui/material/Box';
import { DataGrid, GridToolbarContainer, GridColumnMenu, useGridApiContext, GridEditInputCell } from '@mui/x-data-grid';
import { Button, TextField } from '@mui/material';
import { useCallback, useEffect, useState } from 'react';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import ColorPickerButton from '../ColorPickerButton';
import Role from '../../request/service/Role';
import Event from '../../request/service/Event';

function EditToolbar({ setRoles, event }) {
    const apiRef = useGridApiContext();

    const handleAdd = () => {
        setRoles((oldRows) => {
            if (oldRows.some(row => row.id === -1)) {
                return oldRows; // we do not add a new row if the last added has not been filled
            }

            return [...oldRows, { id: -1, name: '', trust_level: '', permission: '', color: '', persisted: false }];
        });
    };

    const handleDelete = async () => {
        const selectedRows = apiRef.current.getSelectedRows();

        if (selectedRows.size === 0) { // no need to delete if there is no selection
            return;
        }

        const idsToDelete = [...selectedRows.keys()];

        await Promise.all(idsToDelete.map(async (roleId) =>
            await Role.delete(event.id, roleId)
        ));

        setRoles((oldRows) => oldRows.filter(row => !selectedRows.has(row.id)));
    };

    return (
        <GridToolbarContainer>
            <Button color="primary" startIcon={<AddIcon />} onClick={handleAdd}>
                Ajouter un rôle
            </Button>
            <Button color="error" startIcon={<DeleteIcon />} onClick={handleDelete}>
                Supprimer
            </Button>
        </GridToolbarContainer>
    );
}

function CustomColumnMenu(props) {
    return (
        <GridColumnMenu
            {...props}
            slots={{
                columnMenuColumnsItem: null,
            }}
        />
    );
}

export default function RolesEditor({ event, setEvent }) {
    const [roles, setRoles] = useState([]);
    const [trustLevel, setTrustLevel] = useState(event.trust_level_count);

    useEffect(() => {
        async function getRoles() {
            Role.listByEvent(event.id).then(response => setRoles(response));
        }
        getRoles();
    }, [event]);

    const handleTrustLevelsChange = async (newTrustLevel) => {
        await Event.update(event.id, { 'trust_level_count': newTrustLevel });

        setTrustLevel(newTrustLevel);

        event.trust_level_count = newTrustLevel;
        setEvent(event);

        // we update the roles to reduce the trust level if necessary
        roles.forEach((row) => {
            if (row.trust_level >= newTrustLevel) {
                row.trust_level = newTrustLevel - 1;
                handleProcessRowUpdate(row);
            }
        });
    };

    const handleProcessRowUpdate = useCallback(async (newRow) => {
        const missingProperties = Object.values(newRow).some(x => x === null || x === '');

        if (missingProperties) { // the role is not full filled, we do not persist
            return newRow;
        }

        if (newRow.persisted === false) {
            await Role.create(event.id, newRow).then(response => {
                newRow.id = response.id; // replace -1 with the id given by the API
                newRow.persisted = true;
            });
        } else {
            await Role.update(event.id, newRow.id, newRow);
        }

        setRoles(roles.map((row) => [newRow.id, -1].includes(row.id) ? newRow : row));

        return newRow;
    }, [roles, setRoles, event]);

    const columns = [
        {
            field: 'name',
            flex: 1,
            headerName: 'Nom',
            editable: true
        },
        {
            field: 'trust_level',
            flex: 1,
            headerName: 'Niveau de confiance',
            editable: true,
            type: 'number',
            headerAlign: 'left',
            align: 'left',
            renderEditCell: (params) => (
                <GridEditInputCell
                    {...params}
                    inputProps={{
                        max: trustLevel - 1,
                        min: 0,
                    }}
                />
            ),
        },
        {
            field: 'permission',
            flex: 1,
            headerName: 'Permission',
            editable: true,
            type: 'singleSelect',
            valueOptions: [
                { value: 'GUEST', label: 'Invité' },
                { value: 'MEMBER', label: 'Membre' },
                { value: 'MANAGER', label: 'Sous-responsable' },
                { value: 'SUPER_MANAGER', label: 'Responsable' },
                { value: 'ADMIN', label: 'Administrateur' }
            ]
        },
        {
            field: 'color',
            flex: 1,
            headerName: 'Couleur',
            renderCell: (params) => {
                return (
                    <ColorPickerButton {...params} color={params.value} onChange={() => handleProcessRowUpdate(params.row)} />
                );
            },
        }
    ];

    return (
        <Box maxWidth="md" mx="auto">
            <TextField
                label="Nombre de niveaux de confiance"
                type="number"
                inputProps={{ min: 1 }}
                value={trustLevel}
                onChange={(e) => handleTrustLevelsChange(e.target.value)}
                sx={{ mb: 4 }}
            />
            <DataGrid
                autoHeight
                rows={roles}
                columns={columns}
                initialState={{
                    pagination: {
                        paginationModel: {
                            pageSize: 10,
                        },
                    },
                }}
                pageSizeOptions={[5, 10]}
                checkboxSelection
                disableRowSelectionOnClick
                processRowUpdate={handleProcessRowUpdate}
                slots={{
                    toolbar: EditToolbar,
                    columnMenu: CustomColumnMenu
                }}
                slotProps={{
                    toolbar: { setRoles, event },
                }}
                sx={{ mb: 4 }}
            />
        </Box>
    );
}
