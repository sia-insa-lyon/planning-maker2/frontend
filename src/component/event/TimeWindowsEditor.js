import { Box, Button, Grid, TextField } from "@mui/material";
import { DateTimePicker } from "@mui/x-date-pickers";
import { useCallback, useEffect, useState } from "react";
import { DataGrid, GridToolbarContainer, useGridApiContext } from '@mui/x-data-grid';
import { Controller, useForm } from "react-hook-form";
import DeleteIcon from '@mui/icons-material/Delete';
import TimeWindow from '../../request/service/TimeWindow';
import dayjs from "dayjs";
var isSameOrBefore = require('dayjs/plugin/isSameOrBefore');
dayjs.extend(isSameOrBefore);

function generateTimeWindows(startDate, endDate, slotDuration) {
    const timeWindows = [];
    let id = 1;
    let date = startDate;

    while (date.isSameOrBefore(endDate)) {
        const timeWindow = {
            id: id++,
            start_date: date,
            value: 0
        };

        date = date.add(slotDuration, 'minutes');

        timeWindow.end_date = date;
        timeWindows.push(timeWindow);
    }

    return timeWindows;
}

function EditToolbar({ timeWindows, setTimeWindows, event }) {
    const apiRef = useGridApiContext();

    const handleDelete = async () => {
        const selectedRows = apiRef.current.getSelectedRows();

        if (selectedRows.size === 0) { // no need to delete if there is no selection
            return;
        }

        const idsToDelete = [...selectedRows.keys()];

        await Promise.all(idsToDelete.map(async (timeWindowId) =>
            await TimeWindow.delete(event.id, timeWindowId)
        ));

        if (selectedRows.size === timeWindows.length) {
            setTimeWindows(null);
        } else {
            setTimeWindows((oldRows) => oldRows.filter(row => !selectedRows.has(row.id)));
        }
    };

    return (
        <GridToolbarContainer>
            <Button color="error" startIcon={<DeleteIcon />} onClick={handleDelete}>
                Supprimer
            </Button>
        </GridToolbarContainer>
    );
}

export default function TimeWindowsEditor({ event }) {
    const { register, handleSubmit, control, formState: { errors } } = useForm();

    const [timeWindows, setTimeWindows] = useState(null);

    useEffect(() => {
        async function getTimeWindows() {
            TimeWindow.listByEvent(event.id).then(response => {
                setTimeWindows(response.length ? response.map(timeWindow => {
                    timeWindow.start_date = dayjs(timeWindow.start_date);

                    return timeWindow;
                }) : null);
            });
        }
        getTimeWindows();
    }, [event]);

    const handleProcessRowUpdate = useCallback(async (newRow) => {
        await TimeWindow.update(event.id, newRow.id, newRow);
        setTimeWindows(timeWindows.map((row) => (row.id === newRow.id ? newRow : row)));

        return newRow;
    }, [timeWindows, setTimeWindows, event]);

    const onSubmit = async ({ start_date, end_date, time_window_step }) => {
        const newTimeWindows = generateTimeWindows(start_date, end_date, time_window_step);

        // we delete the all timeWindows before adding the new ones
        if (timeWindows) {
            await Promise.all(timeWindows.map(async (timeWindow) =>
                await TimeWindow.delete(event.id, timeWindow.id)
            ));
        }

        await Promise.all(newTimeWindows.map(async (timeWindow) =>
            await TimeWindow.create(event.id, timeWindow).then(response => timeWindow.id = response.id)
        ));

        setTimeWindows(newTimeWindows);
    };

    const columns = [
        { field: 'day', headerName: 'Jour', flex: 1, valueGetter: ({ row }) => row.start_date.format('DD/MM/YYYY') },
        { field: 'time', headerName: 'Heure', flex: 1, valueGetter: ({ row }) => row.start_date.format('HH:mm') },
        { field: 'value', headerName: 'Valeur', type: 'number', headerAlign: 'center', align: 'center', flex: 1, editable: true },
    ];

    return (<>
        <Box component="form" noValidate onSubmit={handleSubmit(onSubmit)} maxWidth="md" mx="auto">
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <Controller
                        name="start_date"
                        control={control}
                        rules={{ required: true }}
                        render={({ field }) => (
                            <DateTimePicker
                                label="Date du premier créneau"
                                onChange={field.onChange}
                                slotProps={{
                                    textField: {
                                        fullWidth: true,
                                        required: true,
                                        error: !!errors.start_date
                                    },
                                }}
                            />
                        )}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Controller
                        name="end_date"
                        control={control}
                        rules={{ required: true }}
                        render={({ field }) => (
                            <DateTimePicker
                                label="Date du dernier créneau"
                                onChange={field.onChange}
                                slotProps={{
                                    textField: {
                                        fullWidth: true,
                                        required: true,
                                        error: !!errors.end_date
                                    },
                                }}
                            />
                        )}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        required
                        label="Pas (en minutes)"
                        type="number"
                        inputProps={{ min: 1, max: 1440 }}
                        error={!!errors.time_window_step}
                        defaultValue={60}
                        {...register("time_window_step", {
                            required: true,
                            valueAsNumber: true,
                        })}
                    />
                </Grid>
            </Grid>
            <Button fullWidth type="submit" variant="contained" sx={{ mt: 1 }}>
                {timeWindows ? "Régénérer" : "Générer"} les créneaux
            </Button>
            {timeWindows &&
                <DataGrid
                    rows={timeWindows}
                    columns={columns}
                    processRowUpdate={handleProcessRowUpdate}
                    initialState={{
                        pagination: {
                            paginationModel: {
                                pageSize: 5,
                            },
                        },
                    }}
                    pageSizeOptions={[5, 10, 15]}
                    disableRowSelectionOnClick
                    checkboxSelection
                    sx={{ mt: 5 }}
                    slots={{
                        toolbar: EditToolbar,
                    }}
                    slotProps={{
                        toolbar: { timeWindows, setTimeWindows, event },
                    }}
                />
            }
        </Box>
    </>
    );
}
