import { privateClient as client } from "../client";

export default {
    list: () => {
        return client.get('event/').then(response => response.data);
    },

    detail: (id) => {
        return client.get(`event/${id}/`).then(response => response.data);
    },

    create: (data) => {
        return client.post(`event/`, data).then(response => response.data);
    },

    update: (id, data) => {
        return client.patch(`event/${id}/`, data).then(response => response.data);
    },

    delete: (id) => {
        return client.delete(`event/${id}/`).then(response => response.data);
    },
};
