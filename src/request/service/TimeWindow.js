import { privateClient as client } from "../client";

export default {
    listByEvent: (eventId) => {
        return client.get(`event/${eventId}/timeWindow/`).then(response => response.data);
    },

    create: (eventId, data) => {
        return client.post(`event/${eventId}/timeWindow/`, data).then(response => response.data);
    },

    update: (eventId, timeWindowId, data) => {
        return client.put(`event/${eventId}/timeWindow/${timeWindowId}/`, data).then(response => response.data);
    },

    delete: (eventId, timeWindowId) => {
        return client.delete(`event/${eventId}/timeWindow/${timeWindowId}/`).then(response => response.data);
    },
};
