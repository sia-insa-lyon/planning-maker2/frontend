import { privateClient as client } from "../client";

export default {
    listByEvent: (eventId) => {
        return client.get(`event/${eventId}/role/`).then(response => response.data);
    },

    create: (eventId, data) => {
        return client.post(`event/${eventId}/role/`, data).then(response => response.data);
    },

    update: (eventId, roleId, data) => {
        return client.put(`event/${eventId}/role/${roleId}/`, data).then(response => response.data);
    },

    delete: (eventId, roleId) => {
        return client.delete(`event/${eventId}/role/${roleId}/`).then(response => response.data);
    },
};
