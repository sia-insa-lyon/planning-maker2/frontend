import React from 'react';
import Home from './Home';
import Login from './Login';
import Event from './Event';
import Signin from './Signin';
import { Routes, Route } from 'react-router-dom';
import ProtectedRoute from '../component/layout/ProtectedRoute';
import NotFound from '../component/NotFound';
import NavBar from '../component/NavBar';
import EventNav from '../component/EventNav';
import EventCreation from './EventCreation';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import 'dayjs/locale/fr';
import EventSettings from './EventSettings';

function App() {
    return (
        <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="fr">
            <div className="App">
                <Routes>
                    <Route element={<ProtectedRoute />}>
                        <Route element={<NavBar />}>
                            <Route index element={<Home />} />
                            <Route path="/evenements/creer" element={<EventCreation />} />
                        </Route>
                        <Route element={<EventNav />}>
                            <Route path="/evenements/:id" element={<Event />} />
                            <Route path="/evenements/:id/planning" />
                            <Route path="/evenements/:id/reglages" element={<EventSettings />} />
                        </Route>
                    </Route>
                    <Route>
                        <Route path="/connexion" element={<Login />} />
                        <Route path="/inscription" element={<Signin />} />
                        <Route path="*" element={<NotFound />} />
                    </Route>
                </Routes>
            </div>
        </LocalizationProvider>
    );
}

export default App;
