import PropTypes from 'prop-types';
import { Box, Button, Grid, Tab, Tabs, TextField } from "@mui/material";
import { useState } from "react";
import { useOutletContext } from "react-router-dom";
import InfoIcon from '@mui/icons-material/Info';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import LocalPoliceIcon from '@mui/icons-material/LocalPolice';
import RolesEditor from '../component/event/RolesEditor';
import TimeWindowsEditor from '../component/event/TimeWindowsEditor';
import { Controller, useForm } from 'react-hook-form';
import { DateTimePicker } from '@mui/x-date-pickers';
import Event from '../request/service/Event';
import dayjs from 'dayjs';
import User from '../request/service/User';
import NotFound from '../component/NotFound';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 5 }}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

function EventInfoForm({ event, setEvent }) {
    const { register, handleSubmit, control, formState: { errors } } = useForm();

    const onSubmit = async (data) => {
        await Event.update(event.id, data).then(response => setEvent(response));
    };

    return (
        <Box component="form" maxWidth="md" mx="auto" noValidate onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        required
                        fullWidth
                        label="Nom de l'évènement"
                        variant="outlined"
                        error={!!errors.name}
                        defaultValue={event.name}
                        {...register("name", { required: true })}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        label="Description de l'évènement"
                        variant="outlined"
                        multiline
                        rows={3}
                        defaultValue={event.description}
                        {...register("description")}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Controller
                        name="start_date"
                        control={control}
                        rules={{ required: true }}
                        defaultValue={dayjs(event.start_date)}
                        render={({ field }) => (
                            <DateTimePicker
                                label="Début de l'évènement"
                                onChange={field.onChange}
                                slotProps={{
                                    textField: {
                                        required: true,
                                        fullWidth: true,
                                        error: !!errors.start_date,
                                        defaultValue: dayjs(event.start_date)
                                    },
                                }}
                            />
                        )}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Controller
                        name="end_date"
                        control={control}
                        rules={{ required: true }}
                        defaultValue={dayjs(event.end_date)}
                        render={({ field }) => (
                            <DateTimePicker
                                label="Fin de l'évènement"
                                onChange={field.onChange}
                                slotProps={{
                                    textField: {
                                        required: true,
                                        fullWidth: true,
                                        error: !!errors.end_date,
                                        defaultValue: dayjs(event.end_date)
                                    },
                                }}
                            />
                        )}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Controller
                        name="start_registration_date"
                        control={control}
                        rules={{ required: true }}
                        defaultValue={dayjs(event.start_registration_date)}
                        render={({ field }) => (
                            <DateTimePicker
                                label="Début des inscriptions"
                                onChange={field.onChange}
                                slotProps={{
                                    textField: {
                                        required: true,
                                        fullWidth: true,
                                        error: !!errors.start_registration_date,
                                        defaultValue: dayjs(event.start_registration_date)
                                    },
                                }}
                            />
                        )}
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Controller
                        name="end_registration_date"
                        control={control}
                        rules={{ required: true }}
                        defaultValue={dayjs(event.end_registration_date)}
                        render={({ field }) => (
                            <DateTimePicker
                                label="Fin des inscriptions"
                                onChange={field.onChange}
                                slotProps={{
                                    textField: {
                                        required: true,
                                        fullWidth: true,
                                        error: !!errors.end_registration_date,
                                        defaultValue: dayjs(event.end_registration_date)
                                    },
                                }}
                            />
                        )}
                    />
                </Grid>
            </Grid>
            <Button fullWidth type="submit" variant="contained" sx={{ mt: 5 }}>
                Sauvegarder
            </Button>
        </Box>
    );
}

function EventSettings() {
    const user = User.get();
    const [event, setEvent] = useOutletContext();
    const [value, setValue] = useState(0);

    if (!user.is_superuser) {
        return <NotFound />;
    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (<>
        <Box sx={{ borderBottom: 1, borderColor: 'lightGrey' }}>
            <Tabs centered value={value} onChange={handleChange} aria-label="basic tabs example">
                <Tab icon={<InfoIcon />} sx={{ pb: 0 }} iconPosition="start" label="Informations générales" {...a11yProps(0)} />
                <Tab icon={<AccessTimeIcon />} sx={{ pb: 0 }} iconPosition="start" label="Créneaux" {...a11yProps(1)} />
                <Tab icon={<LocalPoliceIcon />} sx={{ pb: 0 }} iconPosition="start" label="Rôles" {...a11yProps(2)} />
            </Tabs>
        </Box>
        <TabPanel value={value} index={0}>
            {event && <EventInfoForm event={event} setEvent={setEvent} />}
        </TabPanel>
        <TabPanel value={value} index={1}>
            {event && <TimeWindowsEditor event={event} setEvent={setEvent} />}
        </TabPanel>
        <TabPanel value={value} index={2}>
            {event && <RolesEditor event={event} setEvent={setEvent} />}
        </TabPanel>
    </>
    );
}

export default EventSettings;
