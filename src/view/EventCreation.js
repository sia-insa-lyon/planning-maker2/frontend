import TextField from '@mui/material/TextField';
import { Box, Button, Container, Grid, Stack } from '@mui/material';
import { Typography } from '@mui/material';
import NotFound from '../component/NotFound';
import User from '../request/service/User';
import Event from '../request/service/Event';
import InfoIcon from '@mui/icons-material/Info';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { Controller, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { DateTimePicker } from '@mui/x-date-pickers';

function EventCreation() {
    const user = User.get();
    const { register, handleSubmit, control, formState: { errors } } = useForm();
    const navigate = useNavigate();

    if (!user.is_superuser) {
        return <NotFound />;
    }

    const onSubmit = async (data) => {
        await Event.create(data).then((response) => {
            const eventId = response.id;
            navigate(`/evenements/${eventId}/reglages`);
        });
    };

    return (
        <Container sx={{ my: 6 }} maxWidth="sm">
            <Stack alignItems="center">
                <Typography variant="h3" component="h1" mb={2}>Créer un évènement</Typography>
                <Box component="form" noValidate onSubmit={handleSubmit(onSubmit)}>
                    <Stack direction="row" alignItems="center" mb={1}>
                        <InfoIcon />
                        <Typography component="h2" variant="h6" ml={1}>Informations générales</Typography>
                    </Stack>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                label="Nom de l'évènement"
                                variant="outlined"
                                error={!!errors.name}
                                {...register("name", { required: true })}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Description de l'évènement"
                                variant="outlined"
                                multiline
                                rows={3}
                                {...register("description")}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Stack direction="row" alignItems="center" mt={2}>
                                <CalendarMonthIcon />
                                <Typography component="h2" variant="h6" ml={1}>Dates</Typography>
                            </Stack>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography component="h3" variant="subtitle1">Dates de l'évènement</Typography>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Controller
                                name="start_date"
                                control={control}
                                rules={{ required: true }}
                                render={({ field }) => (
                                    <DateTimePicker
                                        label="Début de l'évènement"
                                        onChange={field.onChange}
                                        slotProps={{
                                            textField: {
                                                fullWidth: true,
                                                required: true,
                                                error: !!errors.start_date
                                            },
                                        }}
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Controller
                                name="end_date"
                                control={control}
                                rules={{ required: true }}
                                render={({ field }) => (
                                    <DateTimePicker
                                        label="Fin de l'évènement"
                                        onChange={field.onChange}
                                        slotProps={{
                                            textField: {
                                                fullWidth: true,
                                                required: true,
                                                error: !!errors.end_date
                                            },
                                        }}
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={12} mt={1}>
                            <Typography component="h3" variant="subtitle1">Dates d'inscription des volontaires</Typography>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Controller
                                name="start_registration_date"
                                control={control}
                                rules={{ required: true }}
                                render={({ field }) => (
                                    <DateTimePicker
                                        label="Début des inscriptions"
                                        onChange={field.onChange}
                                        slotProps={{
                                            textField: {
                                                fullWidth: true,
                                                required: true,
                                                error: !!errors.start_registration_date
                                            },
                                        }}
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Controller
                                name="end_registration_date"
                                control={control}
                                rules={{ required: true }}
                                render={({ field }) => (
                                    <DateTimePicker
                                        label="Fin des inscriptions"
                                        onChange={field.onChange}
                                        slotProps={{
                                            textField: {
                                                fullWidth: true,
                                                required: true,
                                                error: !!errors.end_registration_date
                                            },
                                        }}
                                    />
                                )}
                            />
                        </Grid>
                    </Grid>
                    <Button fullWidth type="submit" variant="contained" sx={{ mt: 5 }}>
                        Créer l'évènement
                    </Button>
                </Box>
            </Stack>
        </Container>
    );
}

export default EventCreation;
