import React from 'react';
import { Avatar, Button, CssBaseline, TextField, Link, Paper, Box, Grid, Typography } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import User from '../request/service/User';

const theme = createTheme();

function Signin() {

    const { register, handleSubmit, formState: { errors, isSubmitted, isSubmitSuccessful } } = useForm();
    const navigate = useNavigate();

    const onSubmit = async (data) => {
        const response = await User.register(data);
        console.log(response);
        //TODO: incrire l'utilisateur
        //TODO : rediriger vers la page d'accueil
        navigate('/');
        //TODO : authentifier l'utilisateur
    };


    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '100vh' }}>
                <CssBaseline />
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={7}
                    sx={{
                        backgroundImage: 'url(/logo.png)',
                        backgroundRepeat: 'no-repeat',
                        backgroundPosition: 'center',
                        backgroundSize:'85%'
                    }}
                />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <Box
                        sx={{
                            my: 8,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Inscription
                        </Typography>
                        <Box component="form" noValidate onSubmit={handleSubmit(onSubmit)} sx={{ mt: 1 }}>
                            <Grid container spacing={2}>

                                <TextField
                                    margin="normal"
                                    autoComplete="fname"
                                    name="firstname"
                                    required
                                    fullWidth
                                    id="firstname"
                                    label="Prénom"
                                    autoFocus
                                    error={!!errors.firstname}
                                    {...register("firstname", { required: true })}
                                />
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="lastname"
                                    label="Nom de famille"
                                    name="lastname"
                                    autoComplete="lname"
                                    error={!!errors.lastname}
                                    {...register("lastname", { required: true })}
                                />
                                <TextField 
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="phone"
                                    label="Téléphone"
                                    name="phone"
                                    autoComplete="phone"
                                    error={!!errors.phone}
                                    {...register("phone", { required: true })}
                                />


                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Adresse email"
                                    name="email"
                                    autoComplete="email"
                                    autoFocus
                                    error={!!errors.email}
                                    {...register("email", { required: true })}
                                />
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Mot de passe"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    error={!!errors.password}
                                    {...register("password", { required: true })}
                                />
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password_confirmation"
                                    label="Confirmation de mot de passe"
                                    type="password"
                                    id="password_confirmation"
                                    autoComplete="current-password"
                                    error={!!errors.password_confirmation}
                                    {...register("password_confirmation", { required: true })}
                                />
                                {isSubmitted && !isSubmitSuccessful
                                    && <Typography align="left" sx={{ mt: 1, color: 'red' }}>Adresse email ou mot de passe incorrect.</Typography>}
                                <Button
                                    type="submit"
                                    fullWidth   
                                    variant="contained"
                                    sx={{ mt: 3, mb: 2 }}
                                >
                                    S'inscrire
                                </Button>
                                <Grid container>
                                    <Grid item xs>
                                        <Link href="/connexion" variant="body2">
                                            Vous avez déjà un compte ? Connectez vous
                                        </Link>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </ThemeProvider>
    );

    /*return (
        <div>
            <h1>Login</h1>
            <form>
                <TextField id="nom" label="Nom" variant="outlined" />
                <TextField id="prenom" label="Prenom" variant="outlined" />
                <TextField id="telephone" label="Téléphone" variant="outlined" />
                <TextField id="email" label="Adresse mail" variant="outlined" />
                <TextField id="password" label="Mot de passe" type="password" autoComplete="current-password" />
                <TextField id="password_confirmation" label="Confirmer le mot de passe" type="password" autoComplete="current-password" />

                <Button variant="contained" onClick={handleClick}>Inscription</Button>
            </form>         
        </div>
    );*/
}

export default Signin;