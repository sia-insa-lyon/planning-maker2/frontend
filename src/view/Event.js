import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import { useOutletContext } from "react-router-dom";

function Event() {
    const event = useOutletContext();

    return (
        <Box m={6}>
            <Typography variant="h3" component="h1">{event.name}</Typography>
        </Box>
    );
}

export default Event;
