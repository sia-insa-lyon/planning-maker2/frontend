import { Button, Container, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material';
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Event from '../request/service/Event';
import User from '../request/service/User';

function Home() {
    const user = User.get();
    const [events, setEvents] = useState([]);
    const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
    const [eventToDelete, setEventToDelete] = useState(null);

    useEffect(() => {
        async function getEvents() {
            Event.list().then(response => setEvents(response));
        }
        getEvents();
    }, []);

    const handleEventDelete = async (eventId) => {
        await Event.delete(eventId);
        setOpenDeleteDialog(false);
        setEvents((events) =>
            events.filter((event) => event.id !== eventId)
        );
    };

    return (
        <Container sx={{ mt: 6 }}>
            <Typography variant="h3" component="h1" mb={5}>Bonjour {user.first_name}</Typography>
            {user.is_superuser ?
                <Stack>
                    <Button
                        component={Link}
                        to="/evenements/creer"
                        variant="contained"
                    >
                        Créer un évènement
                    </Button>
                </Stack>
                : <Typography variant="h5" component="h2" >Voici la liste de tes évènements :</Typography>
            }
            <TableContainer component={Paper} sx={{ my: 2 }}>
                <Table sx={{ minWidth: 650 }} aria-label="tableau d'évènements">
                    <TableHead>
                        <TableRow>
                            <TableCell>Nom</TableCell>
                            <TableCell align="right">Date de début</TableCell>
                            <TableCell align="right">Date de fin</TableCell>
                            <TableCell align="right"></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {events.map((event) => (
                            <TableRow
                                key={event.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {event.name}
                                </TableCell>
                                <TableCell align="right">{new Date(event.start_date).toLocaleString('fr-FR').substring(0, 16)}</TableCell>
                                <TableCell align="right">{new Date(event.end_date).toLocaleString('fr-FR').substring(0, 16)}</TableCell>
                                <TableCell align="right">
                                    <Stack direction="row" justifyContent="flex-end" spacing={2}>
                                        <Button href={`evenements/${event.id}`}>Consulter</Button>
                                        {user.is_superuser &&
                                            <>
                                                <Button href={`evenements/${event.id}/reglages`} color="warning">Éditer</Button>
                                                <Button color="error" onClick={() => { setOpenDeleteDialog(true); setEventToDelete(event.id); }}>Supprimer</Button>
                                            </>
                                        }
                                    </Stack>
                                </TableCell>
                            </TableRow>
                        ))}
                        <Dialog open={openDeleteDialog} onClose={() => setOpenDeleteDialog(false)}>
                            <DialogTitle>Confirmation de suppression</DialogTitle>
                            <DialogContent>
                                <DialogContentText>
                                    Êtes-vous sûr de vouloir supprimer l'évènement ?
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={() => setOpenDeleteDialog(false)}>Annuler</Button>
                                <Button onClick={() => handleEventDelete(eventToDelete)} color="error">Supprimer</Button>
                            </DialogActions>
                        </Dialog>
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    );
}

export default Home;
