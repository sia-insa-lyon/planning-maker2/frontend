# Planning Maker

Website to manage event plannings.
To run this app, you will need to have `npm` on your computer.

## Installation
Clone this repository, copy the `.env.example` as `.env` and run `npm start`.

## Available NPM scripts

### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits. You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run linter`

Check the conformity of the code against the style guideline of the SIA.

## Built With
- [Create-React-App](https://facebook.github.io/create-react-app) &mdash; Our bootstrap utility.
- [ReactJS](https://reactjs.org/) &mdash; Our main front framework.
- [MaterialUI](https://material-ui.com/) &mdash; Our style framework.

And other dependencies you can find in package.json file.

## Licence

TODO
